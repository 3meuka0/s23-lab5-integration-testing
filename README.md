# Lab5 -- Integration testing

## My Specs

```https://script.google.com/macros/s/AKfycbxReuav_j4FMBmT7i6ozd5E9oVjBOC7pKFyp5VlKjTRqmn7hjoeTDuzSayzMzTKKi2W/exec?action=get&&service=getSpec&email=m.seytkaliev@innopolis.university```

        Here is InnoCar Specs:
        Budget car price per minute = 22
        Luxury car price per minute = 61
        Fixed price per km = 13
        Allowed deviations in % = 9
        Inno discount in % = 9

## BVA Table

| Parameter   | Equivalence Classes      |
|------------|--------------------------|
| type       |budget, luxury, nonsence |
| plan       |minute, fixed_price, nonsence |
| distance   |<=0, >0, empty |
| time       |<=0, >0, empty |
| planned_distance|<=0, >0, empty|
| planned_time|<=0, >0, empty|
| inno_discount|yes, no, nonsence|

## Desicion table

|#|  Type  |  Plan  |  Distance  |   Time  |    Planned_distance  |    Planned_time  |    Inno_discount  | State |
|--|--------|--------|--------|--------|--------|--------|--------|--------|
|R1| nonsense | * | * | * | * | * | * | Invalid Request|
|R2| * | nonsense | * | * | * | * | * | Invalid Request|
|R3| * | * | empty | * | * | * | * | Invalid Request|
|R4| * | * | <=0 | * | * | * | * | Invalid Request|
|R5| * | * | * | empty | * | * | * | Invalid Request|
|R6| * | * | * | <=0 | * | * | * | Invalid Request|
|R7| * | * | * | * | empty | * | * | Invalid Request|
|R8| * | * | * | * | <=0 | * | * | Invalid Request|
|R9| * | * | * | * | * | empty | * | Invalid Request|
|R10| * | * | * | * | * | <=0 | * | Invalid Request|
|R11| * | * | * | * | * | * | nonsense | Invalid Request|
|R12|luxury|fixed_price|*|*|*|*|*|Invalid Request|
|R13|luxury|minute|>0|>0|>0|>0|yes|OK|
|R14|luxury|minute|>0|>0|>0|>0|no|OK|
|R15|budget|fixed_price|>0|>0|>0|>0|yes|OK|
|R16|budget|fixed_price|>0|>0|>0|>0|no|OK|
|R17|budget|minute|>0|>0|>0|>0|yes|OK|
|R18|budget|minute|>0|>0|>0|>0|no|OK|

## Tests

```https://script.google.com/macros/s/AKfycbzp093GuTsC8jWmn6v8zWvaUAw0LErgD-P8VXt71Ll8-y2lewGh0Mw6sFyg6truO66T/exec?service=calculatePrice&email=m.seytkaliev@innopolis.university&type=type&plan=plan&distance=distance&planned_distance=planned_distance&time=time&planned_time=planned_time&inno_discount=inno_discount```

|# in DT |  type  |  plan  |  distance  |   planned_distance  |    time  |    planned_time  |    inno_discount  | Expected | Got | Result |
|-|--------|--------|--------|--------|--------|--------|--------|--------|--------|--------|
|R1|type|minute|1|1|1|1|yes|Invalid Request|Invalid Request|__OK__|
|R2|budget|plan|1|1|1|1|yes|Invalid Request|Invalid Request|__OK__|
|R3|budget|fixed_price||1|1|1|no|Invalid Request|{"price":15.166666666666668}|__Domain Error__|
|R4|budget|fixed_price|-1|1|1|1|no|Invalid Request|Invalid Request|__OK__|
|R5|budget|fixed_price|1||1|1|no|Invalid Request|{"price":16.666666666666668}|__Domain Error__|
|R6|budget|fixed_price|1|-1|1|1|no|Invalid Request|Invalid Request|__OK__|
|R7|budget|minute|1|1||1|no|Invalid Request|{"price":0}|__Domain Error__|
|R8|budget|minute|1|1|-1|1|no|Invalid Request|Invalid Request|__OK__|
|R9|budget|fixed_price|1|1|1||no|Invalid Request|{"price":16.666666666666668}|__Domain Error__|
|R10|budget|fixed_price|1|1|1|-1|no|Invalid Request|Invalid Request|__OK__|
|R11|budget|fixed_price|1|1|1|1|inno|Invalid Request|Invalid Request|__OK__|
|R12|luxury|fixed_price|1|1|1|1|no|Invalid Request|Invalid Request|__OK__|
|R13|luxury|minute|1|1|1|1|yes|{"price":55.51}|{"price":55.510000000000005}|__Computational Error__|
|R14|luxury|minute|1|1|1|1|no|{"price":61}|{"price":61}|__OK__|
|R15|budget|fixed_price|1|1|1|1|yes|{"price":20.02}|{"price":11.375}|__Computational Error__|
|R15|budget|fixed_price|2|1|1|1|yes|{"price":20.02}|{"price":15.166666666666668}|__Computational Error__|
|R15|budget|fixed_price|2|1|1|1|no|{"price":22}|{"price":16.666666666666668}|__Computational Error__|
|R15|budget|fixed_price|2|1|2|1|yes|{"price":44}|{"price":30.333333333333336}|__Computational Error__|
|R15|budget|fixed_price|2|1|2|1|no|{"price":40.04}|{"price":33.333333333333336}|__Computational Error__|
|R15|budget|fixed_price|1|1|2|1|yes|{"price":44}|{"price":30.333333333333336}|__Computational Error__|
|R15|budget|fixed_price|1|1|2|1|no|{"price":40.04}|{"price":33.333333333333336}|__Computational Error__|
|R16|budget|fixed_price|1|1|1|1|no|{"price":22}|{"price":12.5}|__Computational Error__|
|R17|budget|minute|1|1|1|1|yes|{"price":20.02}|{"price":26.026000000000003}|__Computational Error__|
|R18|budget|minute|1|1|1|1|no|{"price":22}|{"price":28.6}|__Computational Error__|

## Bugs

1. Queries with empty fields
2. Discount for luxury type computed incorrectly
3. Prices for budget type computed incorrectly

Estimated coverage = 85%
